#include"Class.h"

//GameManager
SDLManager::SDLManager()
{
	isRunning = true;
	isKeyDown = false;
	error = false;
	keyboard = nullptr;
}

void SDLManager::InitWinRendSDL(const std::string& title, int winX, int winY)
{
	InitSDL();
	if (CheckError())
	{
		return;
	}
	InitWin(title, winX, winY);
	if (CheckError())
	{
		return;
	}
	InitRend();
}

void SDLManager::InitSDL()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		SDL_Log("Error : %s\n", SDL_GetError());
		error = true;
	}
}

void SDLManager::InitWin(const std::string& title, int winX, int winY)
{
	screenSizeX = winX;
	screenSizeY = winY;

	win = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, winX, winY, SDL_WINDOW_UTILITY);
	if (win == nullptr)
	{
		SDL_Log("Error : %s\n", SDL_GetError());
		error = true;
	}
}

void SDLManager::InitRend()
{
	graphics = SDL_CreateRenderer(win, 0, SDL_RENDERER_ACCELERATED);
	if (graphics == nullptr)
	{
		SDL_Log("Error : %s\n", SDL_GetError());
		error = true;
	}
}

void SDLManager::CheckEvents()
{
	SDL_PollEvent(&events);

	switch (events.type)
	{
	case SDL_QUIT:
		isRunning = false;
		break;
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		keyboard = SDL_GetKeyboardState(nullptr);
		break;
	}
}

void SDLManager::Close()
{
	SDL_DestroyRenderer(graphics);
	SDL_DestroyWindow(win);
	SDL_Quit();
}

void SDLManager::Clear(unsigned char r, unsigned char g, unsigned char b)
{
	SDL_SetRenderDrawColor(graphics, r, g, b, 255);
	SDL_RenderClear(graphics);
}

void SDLManager::Render()
{
	SDL_RenderPresent(graphics);
}

void SDLManager::start()
{
	while (isRunning)
	{

	}
}

int SDLManager::TakeSreenSizeX()
{
	return screenSizeX;
}

int SDLManager::TakeScreenSizeY()
{
	return screenSizeY;
}

char SDLManager::CheckKeyBoard()
{
	if (keyboard == nullptr || (keyboard[SDL_SCANCODE_A] == 1 && keyboard[SDL_SCANCODE_D] == 1))
	{
		return 0;
	}
	if (keyboard[SDL_SCANCODE_A] == 1)
	{
		return 'a';
	}
	if (keyboard[SDL_SCANCODE_D] == 1)
	{
		return 'd';
	}
}

bool SDLManager::CheckError()
{
	return error;
}

bool SDLManager::CheckIsRunning()
{
	return isRunning;
}

SDL_Renderer* SDLManager::TakeGraphics()
{
	return graphics;
}

//Clock
Clock::Clock() : Clock(60)
{

}

Clock::Clock(unsigned int FPS)
{
	SetFPS(FPS);
}

void Clock::SetFPS(unsigned int FPS)
{
	this->FPS = FPS;
	FRAME_TARGET_TIME = 1000 / FPS;
}

void Clock::SetStart()
{
	start = clock();
}

void Clock::SetEnd()
{
	end = clock();
}

void Clock::Wait()
{
	rest = start + FRAME_TARGET_TIME - end;

	if (rest > 0)
	{
		Sleep(rest);
	}
}

//Breakout
void Breakout::TakeHit(int amount)
{
	health -= amount;
	if (health < 0)
	{
		health = 0;
	}
}

//Brick
Brick::Brick()
{
	width = 50;
	height = 25;
	health = 10;
}

Brick::Brick(int width, int height, int health)
{
	this->width = width;
	this->height = height;
	this->health = health;
}

void Brick::SetPosition(int x, int y)
{
	vect.Set(x, y);
}

void Brick::Draw(SDL_Renderer* graphics, unsigned char r, unsigned char g, unsigned char b, char degraderR, char degraderG, char degraderB, unsigned char grosseurStep, unsigned char nombreStep)
{
	int a = 0;
	SDL_Rect rect = { vect.TakeX(), vect.TakeY(), width, height };
	for (int i = 0; i < nombreStep; i++)
	{
		SDL_Rect rect = { vect.TakeX() + a, vect.TakeY() + a, width - (a * 2), height - a};
		if (health > 0)
		{
			SDL_SetRenderDrawColor(graphics, r, g, b, 255);
			SDL_RenderFillRect(graphics, &rect);
		}
		r += degraderR;
		g += degraderG;
		b += degraderB;
		a += grosseurStep;
	}
}

void Brick::TakeHit(int amount)
{
	health -= amount;
	if (health < 0)
	{
		health = 0;
	}
}

void Brick::SetSize(int width, int height)
{
	this->width = width;
	this->height = height;
}

int Brick::TakeX()
{
	return vect.TakeX();
}

int Brick::TakeY()
{
	return vect.TakeY();
}

int Brick::TakeWidth()
{
	return width;
}

int Brick::TakeHeight()
{
	return height;
}

void Brick::SetInitialHealt(int health)
{
	this->health = health;
}

//Ball
Ball::Ball()
{
	width = 10;
	height = 10;
	speed = 5;
	dir_x = 1;
	dir_y = 1;
}

Ball::Ball(int width, int height, int speed)
{
	this->width = width;
	this->height = height;
	this->speed = speed;
	dir_x = 1;
	dir_y = 1;
}

void Ball::move(int screenSizeX, int screenSizeY)
{
	vect.Set(vect.TakeX() + (speed * dir_x), vect.TakeY() + (speed * dir_y));

	if (vect.TakeX() >= screenSizeX - width || vect.TakeX() < 0)
	{
		dir_x = -dir_x;
	}
	if (vect.TakeY() >= screenSizeY - height || vect.TakeY() < 0)
	{
		dir_y = -dir_y;
	}
}

void Ball::SetPosition(int x, int y)
{
	vect.Set(x, y);
}

void Ball::Draw(SDL_Renderer* graphics, unsigned char r, unsigned char g, unsigned char b)
{
	SDL_Rect rect = { vect.TakeX(), vect.TakeY(), width, height };
	SDL_SetRenderDrawColor(graphics, r, g, b, 255);
	SDL_RenderFillRect(graphics, &rect);
}

bool Ball::CheckCollision(Brick* brick)
{
	return false;
}

bool Ball::CheckCollision(Paddle* paddle)
{
	return false;
}

void Ball::SetSize(int width, int height)
{
	this->width = width;
	this->height = height;
}

void Ball::SetSpeed(int speed)
{
	this->speed = speed;
}

//Paddle
Paddle::Paddle()
{
	width = 100;
	height = 25;
	speed = 10;
}

Paddle::Paddle(int width, int height, int speed)
{
	this->width = width;
	this->height = height;
	this->speed = speed;
}

void Paddle::Draw(SDL_Renderer* graphics, unsigned char r, unsigned char g, unsigned char b, unsigned char rC, unsigned char gC, unsigned char bC)
{
	int coinX = width / 10;
	int coinY = height / 3;

	SDL_Rect rect = { vect.TakeX(), vect.TakeY(), width, height };
	SDL_SetRenderDrawColor(graphics, r, g, b, 255);
	SDL_RenderFillRect(graphics, &rect);

	rect = { vect.TakeX(), vect.TakeY(), coinX, coinY };
	SDL_SetRenderDrawColor(graphics, rC, gC, bC, 255);
	SDL_RenderFillRect(graphics, &rect);

	rect = { vect.TakeX(), vect.TakeY() + height - coinY, coinX, coinY };
	SDL_SetRenderDrawColor(graphics, rC, gC, bC, 255);
	SDL_RenderFillRect(graphics, &rect);

	rect = { vect.TakeX() + width - coinX, vect.TakeY(), coinX, coinY };
	SDL_SetRenderDrawColor(graphics, rC, gC, bC, 255);
	SDL_RenderFillRect(graphics, &rect);

	rect = { vect.TakeX() + width - coinX, vect.TakeY() + height - coinY, coinX, coinY };
	SDL_SetRenderDrawColor(graphics, rC, gC, bC, 255);
	SDL_RenderFillRect(graphics, &rect);

}

void Paddle::SetPosition(int x, int y)
{
	vect.Set(x, y);
}

void Paddle::Move(int dir, int screenSizeX, int screenSizeY)
{
	if (dir == 1 && vect.TakeX() < screenSizeX - width)
	{
		vect.Set(vect.TakeX() + speed, vect.TakeY());
	}
	if (dir == -1 && vect.TakeX() > 0)
	{
		vect.Set(vect.TakeX() - speed, vect.TakeY());
	}
}

void Paddle::SetSize(int width, int height)
{
	this->width = width;
	this->height = height;
}

void Paddle::SetSpeed(int speed)
{
	this->speed = speed;
}

//Maps
void Maps::SetVector(Brick brick, int nombreX, int nombreY)
{
	int total = nombreX * nombreY;
	this->nombreX = nombreX;
	this->nombreY = nombreY;

	for (int i = 0; i < total; i++)
	{
		brickMap.emplace_back(brick);
	}
}

void Maps::SetDistanceBrick(int distance)
{
	distanceBrick = distance;
}

void Maps::SetPosition()
{
	int emplacement = 0;
	int positionX = brickMap.at(0).TakeX();
	int positionY = brickMap.at(0).TakeY();

	for (int a = 0; a < nombreY; a++)
	{
		positionX = brickMap.at(0).TakeX();
		if (a != 0)
		{
			positionY += (brickMap.at(0).TakeHeight() + distanceBrick);
		}
		for (int b = 0; b < nombreX; b++)
		{
			if (b != 0)
			{
				positionX += (brickMap.at(0).TakeWidth() + distanceBrick);
			}
			brickMap.at(emplacement).SetPosition(positionX, positionY);
			emplacement++;
		}
	}
}

void Maps::DrawMap(SDL_Renderer* graphics, int screenSizeX, int sreenSizeY, unsigned char r, unsigned char g, unsigned char b, char degraderR, char degraderG, char degraderB, unsigned char grosseurStep, unsigned char nombreStep)
{
	int emplacement = 0;
	int total = nombreX * nombreY;
	for (int i = 0; i < total; i++)
	{
		brickMap.at(emplacement).Draw(graphics, r, g, b, degraderR, degraderG, degraderB, grosseurStep, nombreStep);
		emplacement++;
	}
}

//Vector
Vector::Vector() : x(0), y(0)
{
	SDL_Log("Vector Default Ctor");
}

Vector::Vector(int x, int y)
{
	SDL_Log("Vector Params Ctor");
	Set(x, y);
}

Vector::Vector(const Vector& other)
{
	SDL_Log("Copy CTor");
	x = other.x;
	y = other.y;
}

Vector::~Vector()
{
	SDL_Log("DTor");
}

void Vector::Set(int x, int y)
{
	this->x = x;
	this->y = y;
}

void Vector::Show()
{
	SDL_Log("%f, %f", x, y);
}

int Vector::TakeX()
{
	return x;
}

int Vector::TakeY()
{
	return y;
}
