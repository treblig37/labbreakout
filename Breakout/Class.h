#pragma once
#include<Windows.h>
#include<SDL.h>
#include<time.h>
#include<math.h>
#include<stdlib.h>
#include<iostream>
#include <vector>
#include<string>

using namespace std;

struct SDL_Window;

class SDLManager
{
private:
	unsigned int screenSizeX;
	unsigned int screenSizeY;

	bool isRunning;
	bool isKeyDown;
	bool error;

	const unsigned char* keyboard;

	SDL_Event events;
	SDL_Window* win;
	SDL_Renderer* graphics;

	void InitSDL();
	void InitWin(const std::string& title, int winX, int winY);
	void InitRend();
public:
	SDLManager();
	void InitWinRendSDL(const std::string& title, int winX, int winY);
	void CheckEvents();
	void Close();
	void Clear(unsigned char r, unsigned char g, unsigned char b);
	void Render();

	void start();

	int TakeSreenSizeX();
	int TakeScreenSizeY();

	char CheckKeyBoard();

	bool CheckError();
	bool CheckIsRunning();

	SDL_Renderer* TakeGraphics();
};

class Clock
{
private:
	unsigned int FPS;
	unsigned int FRAME_TARGET_TIME;

	clock_t start;
	clock_t end;

	int rest;
public:
	Clock();
	Clock(unsigned int FPS);
	void SetFPS(unsigned int FPS);
	void SetStart();
	void SetEnd();
	void Wait();
};

class Breakout
{
private:
	int health;
public:
	void TakeHit(int amount);
};

class Vector
{
private:
	int x;
	int y;
public:
	Vector();
	Vector(int x, int y);
	Vector(const Vector& other);
	~Vector();

	void Set(int x, int y);
	void Show();
	int TakeX();
	int TakeY();
};

class Brick
{
private:
	Vector vect;
	int width;
	int height;
	int health;
public:
	Brick();
	Brick(int width, int height, int health);
	void SetInitialHealt(int health);
	void SetPosition(int x, int y);
	void Draw(SDL_Renderer* graphics, unsigned char r, unsigned char g, unsigned char b, char degraderR, char degraderG, char degraderB, unsigned char grosseurStep, unsigned char nombreStep);
	void TakeHit(int amount);
	void SetSize(int width, int height);
	
	int TakeX();
	int TakeY();
	int TakeWidth();
	int TakeHeight();
};

class Paddle
{
private:
	Vector vect;
	int width;
	int height;
	int speed;
public:
	Paddle();
	Paddle(int width, int height, int speed);
	void Draw(SDL_Renderer* graphics, unsigned char r, unsigned char g, unsigned char b, unsigned char rC, unsigned char gC, unsigned char bC);
	void SetPosition(int x, int y);
	void Move(int dir, int screenSizeX, int screenSizeY);
	void SetSize(int width, int height);
	void SetSpeed(int speed);
};

class Ball
{
private:
	Vector vect;
	int width;
	int height;
	int speed;
	int dir_x = 1;
	int dir_y = 1;

public:
	Ball();
	Ball(int width, int height, int speed);
	void move(int screenSizeX, int screenSizeY);
	void SetPosition(int x, int y);
	void Draw(SDL_Renderer* graphics, unsigned char r, unsigned char g, unsigned char b);
	bool CheckCollision(Brick* brick);
	bool CheckCollision(Paddle* paddle);
	void SetSize(int width, int height);
	void SetSpeed(int speed);
};

class Maps
{
private:
	int nombreX;
	int nombreY;
	int distanceBrick;
	vector<Brick> brickMap;
public:
	void SetVector(Brick brick, int nombreX, int nombreY);
	void SetDistanceBrick(int distance);
	void SetPosition();
	void DrawMap(SDL_Renderer* graphics, int screenSizeX, int sreenSizeY, unsigned char r, unsigned char g, unsigned char b, char degraderR, char degraderG, char degraderB, unsigned char grosseurStep, unsigned char nombreStep);
};