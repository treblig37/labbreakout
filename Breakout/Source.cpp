#include"Class.h"

int WINAPI WinMain(HINSTANCE h, HINSTANCE i, LPSTR c, int n)
{
	SDLManager gameManager;
	Clock clock(60);

	Brick brick;
	Paddle paddle;
	Ball ball;
	Maps maps;

	gameManager.InitWinRendSDL("Breakout", 600, 800);
	if (gameManager.CheckError())
	{
		return 1;
	}

	brick.SetPosition(80, 100);

	maps.SetVector(brick, 8, 4);
	maps.SetDistanceBrick(5);
	maps.SetPosition();

	paddle.SetPosition(250, 650);

	ball.SetPosition(295, 600);

	while (gameManager.CheckIsRunning())
	{
		clock.SetStart();

		gameManager.CheckEvents();

		if (gameManager.CheckKeyBoard() != 0)
		{
			if (gameManager.CheckKeyBoard() == 'a')
			{
				paddle.Move(-1, gameManager.TakeSreenSizeX(), gameManager.TakeScreenSizeY());
			}
			if (gameManager.CheckKeyBoard() == 'd')
			{
				paddle.Move(1, gameManager.TakeSreenSizeX(), gameManager.TakeScreenSizeY());
			}
		}

		ball.move(gameManager.TakeSreenSizeX(), gameManager.TakeScreenSizeY());

		gameManager.Clear(65, 65, 65);

		paddle.Draw(gameManager.TakeGraphics(), 255, 100, 0, 0, 100, 255);
		maps.DrawMap(gameManager.TakeGraphics(), gameManager.TakeSreenSizeX(), gameManager.TakeScreenSizeY(), 0, 0, 255, 0, 100, -21, 2, 12);
		ball.Draw(gameManager.TakeGraphics(), 255, 255, 255);

		gameManager.Render();

		clock.SetEnd();
		clock.Wait();
	}

	gameManager.Close();

	SDL_Log("END PROGRAM");
	return 0;
}